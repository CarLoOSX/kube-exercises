> [Horizontal Pod Autoscaler]Crea un objeto de kubernetes HPA, que escale a partir de las métricas CPU o memoria (a vuestra elección). Establece el umbral al 50% de CPU/memoria utilizada, cuando pase el umbral, automáticamente se deberá escalar al doble de replicas.

* Podéis realizar una prueba de estrés realizando un número de peticiones masivas mediante la siguiente instrucción:

    kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://<svc_name>; done"

Para este servicio utilizaré el deployment de nginx-server del hw-02 ex 4:
 
```
kubectl apply -f ../hw-02/deployment4.yml
deployment.apps/nginx-deployment created

kubectl get pods                          
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-f6fbbb77c-6hnh2   1/1     Running   0          40s
nginx-deployment-f6fbbb77c-vqw9k   1/1     Running   0          40s
nginx-deployment-f6fbbb77c-wrqzd   1/1     Running   0          41s
```
Con el service del ejercicio 1 de esta práctica:
```
kubectl apply -f service1.yml           
service/nginx-service created

kubectl describe service | grep app  
Selector:                 app=nginx-server
```
y el ingress-controller para facilitarme la tarea de hacer las peticiones del ataque DOS:

```
kubectl apply -f ingress1.yml 
ingress.networking.k8s.io/nginx-ingress created


kubectl describe ingress 
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
Name:             nginx-ingress
Namespace:        default
Address:          192.168.99.100
Default backend:  default-http-backend:80 (<error: endpoints "default-http-backend" not found>)
TLS:
  tls-secret terminates carlos.student.lasalle.com
Rules:
  Host                        Path  Backends
  ----                        ----  --------
  carlos.student.lasalle.com  
                              /   nginx-service:80   172.17.0.2:80,172.17.0.3:80,172.17.0.4:80)
Annotations:                  nginx.ingress.kubernetes.io/rewrite-target: /$1
Events:
  Type    Reason  Age   From                      Message
  ----    ------  ----  ----                      -------
  Normal  CREATE  31s   nginx-ingress-controller  Ingress default/nginx-ingress
  Normal  UPDATE  22s   nginx-ingress-controller  Ingress default/nginx-ingress
```
Como vemos todo cuadra:

```
kubectl describe pods | grep IP:
IP:           172.17.0.3
  IP:           172.17.0.3
IP:           172.17.0.4
  IP:           172.17.0.4
IP:           172.17.0.2
  IP:           172.17.0.2
```
Ahora vamos a aplicar nuestra estrategia, de escalar cuando la cpu llege al 50% hasta un máximo de 6 réplicas:

```
kubectl autoscale deployment nginx-deployment --cpu-percent=50 --min=3 --max=6

horizontalpodautoscaler.autoscaling/nginx-deployment autoscaled
```

Abrimos pestaña nueva en el terminal y empezamos el ataque:

```
kubectl run -i --tty load-generator --rm --image=busybox --restart=Never -- /bin/sh -c "while sleep 0.01; do wget -q -O- http://carlos.student.lasalle.com; done"
```

La prueba ha ido mal:
![alt text](imagenes/redirect.png)

Por https si que me ha funcionado y vemos como escala perfectamente:
![alt text](imagenes/hpa.png)

Ahora bien he tenido que modificar el deployment bajándole recursos porque sino no había manera de que escalase:

```
 resources:
            limits:
              cpu: "10m"
              memory: "16Mi"
            requests:
              cpu: "10m"
              memory: "16Mi"
```
Lo bonito de esto es que si cortamos el proceso o el pod que hace el ataque vemos como decrece a su mínimo 3 ráplicas originales:

![alt text](imagenes/delete.png)
