>[StatefulSet] Crear un StatefulSet con 3 instancias de MongoDB (ejemplo visto en clase)

* Habilitar el clúster de MongoDB

Primero debemos crear un servicio headless sin ip ya q lo que va a hacer es gestionar las ips de los pods:

```
apiVersion: v1
kind: Service
metadata:
  name: mongodb-svc
  labels:
    app: db
    name: mongodb
spec:
  clusterIP: None
  selector:
    app: db
    name: mongodb
  ports:
  - port: 27017
    targetPort: 27017
```
Comprobamos:

```
kubectl apply -f headless.yml 
service/mongodb-svc created

kubectl get svc 
NAME          TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)     AGE
kubernetes    ClusterIP   10.96.0.1    <none>        443/TCP     46s
mongodb-svc   ClusterIP   None         <none>        27017/TCP   20s
```

Creamos un StorageClass 

Creamos StatefulSet, con replicaset y unos comandos para su inicialización con su StorageClass:
Para esto me he ayudado bastante de internet y la documentación de k8s.
```
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: mongo
spec:
  selector:
    matchLabels:
      app: db
      name: mongodb
  serviceName: mongodb-svc #the name of the headless service
  replicas: 3
  template:
    metadata:
      labels:
        app: db
        name: mongodb
    spec:
      terminationGracePeriodSeconds: 10 #This is important for databases
      containers:
        - name: mongo
          image: mongo:3.6
          command: #https://docs.mongodb.com/manual/tutorial/deploy-replica-set/#start-each-member-of-the-replica-set-with-the-appropriate-options
            - mongod
          args:
            - --bind_ip=0.0.0.0
            - --replSet=rs0 #The name of the replica set that the mongod is part of. All hosts in the replica set must have the same set name.
            - --dbpath=/data/db
          livenessProbe:
            exec:
              command:
                - mongo
                - --eval
                - "db.adminCommand('ping')"
          ports:
            - containerPort: 27017
          volumeMounts:
            - name: mongo-storage
              mountPath: /data/db
  volumeClaimTemplates:
    - metadata:
        name: mongo-storage
      spec:
        storageClassName: "storage-mongo"
        accessModes: ["ReadWriteOnce"]
        resources:
          requests:
            storage: 1Gi
```

Comprobamos :

```
kubectl apply -f StatefulSet.yml 
statefulset.apps/mongo created

```
Aquí me gustaría comentar que he tenido bastantes problemas por la persistencia, ya que por un error del primer despliegue del statefulset, he tardado casi 1 hora en encontrar el error, todos los pods se creaban bien (mirando describe del statefulset)
pero al haber creado mal el pvc del principio por mucho que redesplegase no pasaban de estado waiting.
Después de eliminar todas las pruebas que había hecho de los pvc, al final lo he redesplegado y ha funcionado.

```
kubectl get statefulset                 
NAME    READY   AGE
mongo   0/3     18s

kubectl get statefulset   
 NAME    READY   AGE
 mongo   2/3     81s
```

Como se puede ver, he tenido que jugar con los recursos de los pods para que esto se mantenga en pie ya que el comando de liveness sino lo daba por fallido:

```
kubectl get statefulset                    
NAME    READY   AGE
mongo   2/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   2/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   2/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   2/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   2/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   1/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   1/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   1/3     10m
kubectl get statefulset                    
NAME    READY   AGE
mongo   1/3     10m
                                           
kubectl get pods                         
NAME      READY   STATUS             RESTARTS   AGE
mongo-0   0/1     CrashLoopBackOff   7          14m
mongo-1   0/1     CrashLoopBackOff   7          13m
mongo-2   0/1     CrashLoopBackOff   7          12m
```

Así que he optado por no limitar recursos y por he vuelto a regenerar todo:

![alt text](imagenes/regenerate.png)

Comprobamos tambien sus storages:

```
kubectl get pvc                
NAME                    STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
mongo-storage-mongo-0   Bound    pvc-cf96810c-6061-4d22-93d2-435624080ca6   1Gi        RWO            standard       8m4s
mongo-storage-mongo-1   Bound    pvc-6060c7ca-02e1-49b4-9e00-c7c65721ac3f   1Gi        RWO            standard       7m59s
mongo-storage-mongo-2   Bound    pvc-204d5af9-7a02-4e1a-b70c-dcbcec51f3dd   1Gi        RWO            standard       7m55s
```

Y ya se puede ver que están corriendo perfectamente.

Ahora vamos a comprobar las ips de los nodos o pods asociadas al servicio:

```
kubectl describe endpoints mongodb-svc
Name:         mongodb-svc
Namespace:    default
Labels:       app=db
              name=mongodb
              service.kubernetes.io/headless=
Annotations:  endpoints.kubernetes.io/last-change-trigger-time: 2020-11-22T21:40:24Z
Subsets:
  Addresses:          172.17.0.2,172.17.0.3,172.17.0.4


```

Después comprobamos que los nodos tienen visibilidad entre sí:

```
ubectl exec -it mongo-0 bash
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.

root@mongo-0:/# ping mongo-1.mongodb-svc

PING mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3) 56(84) bytes of data.
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=1 ttl=64 time=0.179 ms
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=2 ttl=64 time=0.063 ms
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=3 ttl=64 time=0.042 ms
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=4 ttl=64 time=0.057 ms
^O64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=5 ttl=64 time=0.075 ms
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=6 ttl=64 time=0.070 ms
64 bytes from mongo-1.mongodb-svc.default.svc.cluster.local (172.17.0.3): icmp_seq=7 ttl=64 time=0.056 ms
^C
--- mongo-1.mongodb-svc.default.svc.cluster.local ping statistics ---
7 packets transmitted, 7 received, 0% packet loss, time 6107ms
rtt min/avg/max/mdev = 0.042/0.077/0.179/0.043 ms
root@mongo-0:/# 

```

Lo siguiente es inicializar el replicaset:

```
kubectl exec -it mongo-0 bash 

root@mongo-0:/# mongo

MongoDB shell version v3.6.21
connecting to: mongodb://127.0.0.1:27017/?gssapiServiceName=mongodb
Implicit session: session { "id" : UUID("8fc9028f-57e7-452f-88cb-0ab51b631199") }
MongoDB server version: 3.6.21
Server has startup warnings: 
2020-11-22T21:40:15.246+0000 I STORAGE  [initandlisten] 
2020-11-22T21:40:15.246+0000 I STORAGE  [initandlisten] ** WARNING: Using the XFS filesystem is strongly recommended with the WiredTiger storage engine
2020-11-22T21:40:15.246+0000 I STORAGE  [initandlisten] **          See http://dochub.mongodb.org/core/prodnotes-filesystem
2020-11-22T21:40:15.828+0000 I CONTROL  [initandlisten] 
2020-11-22T21:40:15.828+0000 I CONTROL  [initandlisten] ** WARNING: Access control is not enabled for the database.
2020-11-22T21:40:15.828+0000 I CONTROL  [initandlisten] **          Read and write access to data and configuration is unrestricted.
2020-11-22T21:40:15.828+0000 I CONTROL  [initandlisten] ** WARNING: You are running this process as the root user, which is not recommended.
2020-11-22T21:40:15.828+0000 I CONTROL  [initandlisten] 
> rs.initiate({_id: "rs0", version: 1, members: [
...   { _id: 0, host : "mongo-0.mongodb-svc.default.svc.cluster.local:27017" },
...   { _id: 1, host : "mongo-1.mongodb-svc.default.svc.cluster.local:27017" },
...   { _id: 2, host : "mongo-2.mongodb-svc.default.svc.cluster.local:27017" }
... ]});
{
	"ok" : 1,
	"operationTime" : Timestamp(1606083446, 1),
	"$clusterTime" : {
		"clusterTime" : Timestamp(1606083446, 1),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	}
}
rs0:SECONDARY> 
```

y guardamos:

```
rs.conf()
```

Aquí cabe decir que este proceso se suele automatizar y como he visto por internet lo que hacen es meter directamente estas instrucciones en un fichero a parte. 
Como se dijo en clase un SideCar (volumen tipo config map).

Comprobamos que estan los 3 configurados:
```
rs0:PRIMARY> rs.status()
{
	"set" : "rs0",
	"date" : ISODate("2020-11-22T22:20:29.768Z"),
	"myState" : 1,
	"term" : NumberLong(1),
	"syncingTo" : "",
	"syncSourceHost" : "",
	"syncSourceId" : -1,
	"heartbeatIntervalMillis" : NumberLong(2000),
	"optimes" : {
		"lastCommittedOpTime" : {
			"ts" : Timestamp(1606083615, 4),
			"t" : NumberLong(1)
		},
		"readConcernMajorityOpTime" : {
			"ts" : Timestamp(1606083615, 4),
			"t" : NumberLong(1)
		},
		"appliedOpTime" : {
			"ts" : Timestamp(1606083615, 4),
			"t" : NumberLong(1)
		},
		"durableOpTime" : {
			"ts" : Timestamp(1606083615, 4),
			"t" : NumberLong(1)
		}
	},
	"members" : [
		{
			"_id" : 0,
			"name" : "mongo-0.mongodb-svc.default.svc.cluster.local:27017",
			"health" : 1,
			"state" : 1,
			"stateStr" : "PRIMARY",
			"uptime" : 2414,
			"optime" : {
				"ts" : Timestamp(1606083615, 4),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2020-11-22T22:20:15Z"),
			"syncingTo" : "",
			"syncSourceHost" : "",
			"syncSourceId" : -1,
			"infoMessage" : "",
			"electionTime" : Timestamp(1606083458, 1),
			"electionDate" : ISODate("2020-11-22T22:17:38Z"),
			"configVersion" : 1,
			"self" : true,
			"lastHeartbeatMessage" : ""
		},
		{
			"_id" : 1,
			"name" : "mongo-1.mongodb-svc.default.svc.cluster.local:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 182,
			"optime" : {
				"ts" : Timestamp(1606083615, 4),
				"t" : NumberLong(1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1606083615, 4),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2020-11-22T22:20:15Z"),
			"optimeDurableDate" : ISODate("2020-11-22T22:20:15Z"),
			"lastHeartbeat" : ISODate("2020-11-22T22:20:28.532Z"),
			"lastHeartbeatRecv" : ISODate("2020-11-22T22:20:29.197Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "mongo-0.mongodb-svc.default.svc.cluster.local:27017",
			"syncSourceHost" : "mongo-0.mongodb-svc.default.svc.cluster.local:27017",
			"syncSourceId" : 0,
			"infoMessage" : "",
			"configVersion" : 1
		},
		{
			"_id" : 2,
			"name" : "mongo-2.mongodb-svc.default.svc.cluster.local:27017",
			"health" : 1,
			"state" : 2,
			"stateStr" : "SECONDARY",
			"uptime" : 182,
			"optime" : {
				"ts" : Timestamp(1606083615, 4),
				"t" : NumberLong(1)
			},
			"optimeDurable" : {
				"ts" : Timestamp(1606083615, 4),
				"t" : NumberLong(1)
			},
			"optimeDate" : ISODate("2020-11-22T22:20:15Z"),
			"optimeDurableDate" : ISODate("2020-11-22T22:20:15Z"),
			"lastHeartbeat" : ISODate("2020-11-22T22:20:28.532Z"),
			"lastHeartbeatRecv" : ISODate("2020-11-22T22:20:29.197Z"),
			"pingMs" : NumberLong(0),
			"lastHeartbeatMessage" : "",
			"syncingTo" : "mongo-0.mongodb-svc.default.svc.cluster.local:27017",
			"syncSourceHost" : "mongo-0.mongodb-svc.default.svc.cluster.local:27017",
			"syncSourceId" : 0,
			"infoMessage" : "",
			"configVersion" : 1
		}
	],
	"ok" : 1,
	"operationTime" : Timestamp(1606083615, 4),
	"$clusterTime" : {
		"clusterTime" : Timestamp(1606083615, 4),
		"signature" : {
			"hash" : BinData(0,"AAAAAAAAAAAAAAAAAAAAAAAAAAA="),
			"keyId" : NumberLong(0)
		}
	}
}

```
* Realizar una operación en una de las instancias a nivel de configuración y
verificar que el cambio se ha aplicado al resto de instancias

Como se dijo por Slack, crearé un usuario y le asignaré un rol, luego comprobare desde otro mongo:

```
rs0:PRIMARY> db.createUser({user:"carlos",pwd:"carlos",roles:[{role: "userAdminAnyDatabase",db:"admin"}]})
Successfully added user: {
	"user" : "carlos",
	"roles" : [
		{
			"role" : "userAdminAnyDatabase",
			"db" : "admin"
		}
	]
}

kubectl exec -it mongo-1 bash  
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.
root@mongo-1:/# mongo


rs0:SECONDARY> show users
2020-11-22T22:31:45.330+0000 E QUERY    [thread1] Error: not master and slaveOk=false :
_getErrorWithCode@src/mongo/shell/utils.js:25:13
DB.prototype.getUsers@src/mongo/shell/db.js:1709:1
shellHelper.show@src/mongo/shell/utils.js:843:9
shellHelper@src/mongo/shell/utils.js:750:15
@(shellhelp2):1:1

rs0:SECONDARY> rs.slaveOk()
WARNING: slaveOk() is deprecated and may be removed in the next major release. Please use secondaryOk() instead.
rs0:SECONDARY> rs.secondaryOk()

rs0:SECONDARY> show users
{
	"_id" : "test.carlos",
	"userId" : UUID("7941722d-2d3c-4535-a1e8-c4cdc546025a"),
	"user" : "carlos",
	"db" : "test",
	"roles" : [
		{
			"role" : "userAdminAnyDatabase",
			"db" : "admin"
		}
	]
}
rs0:SECONDARY> 
```
Como se puede ver he creado un usuario en mongo-0 y lo he consultado en mongo-1.

* Diferencias que existiría si el montaje se hubiera realizado con el objeto de
ReplicaSet

La diferencia clave es la persistencia. Los deployments con replicaset están pensados para ser stateless.
 
Un claro ejemplo es el de este ejercicio, para algo duradero en el tiempo y persistente como es una BD utilizaría statefulset, de lo contrario, para algo volátil como puede ser 
una api que sirva simplemente a peticiones utilizaría un deployment.