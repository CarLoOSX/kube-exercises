> [Ingress Controller / Secrets] Crea los siguientes objetos de forma declarativa con las siguientes especificaciones:

```
• Imagen: nginx
• Version: 1.19.4
• 3 replicas
• Label: app: nginx-server
• Exponer el puerto 80 de los pods
• Limits:
CPU: 20 milicores Memoria: 128Mi
• Requests:
CPU: 20 milicores Memoria: 128Mi
```

Creamos nuestro deployment:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          resources:
            limits:
              cpu: "20m"
              memory: "128Mi"
            requests:
              cpu: "20m"
              memory: "128Mi"
```
Comprobamos:
```
kubectl get deployments
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           45m
```

Creamos el servicio que expondrá el puerto:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx-service
  namespace: default
spec:
  type: NodePort
  ports:
    - name: http
      port: 80
  selector:
    app: nginx-server


```
Comprobamos:
```
kubectl get svc                               master 
NAME            TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)        AGE
kubernetes      ClusterIP   10.96.0.1       <none>        443/TCP        5d20h
nginx-service   NodePort    10.105.121.60   <none>        80:31185/TCP   44m

```

* A continuación, tras haber expuesto el servicio en el puerto 80, se deberá acceder a la página principal de Nginx a través de la siguiente URL:
 
  http://<student_name>.student.lasalle.com
  
Aquí lo que he hecho es habilitar directamente el ingress controller de NGINX:

```
minikube addons enable ingress
```
  También, he creado un ingress1.yml que contiene lo siguiente:
  
  ```
  apiVersion: networking.k8s.io/v1
  kind: Ingress
  metadata:
    name: nginx-ingress
    annotations:
      nginx.ingress.kubernetes.io/rewrite-target: /$1
  spec:
    rules:
      - host: carlos.student.lasalle.com
        http:
          paths:
            - path: /
              pathType: Prefix
              backend:
                service:
                  name: nginx-service
                  port:
                    number: 80
  ```

Podemos comprobar que el ingress está corriendo de la siguiente manera:

```
kubectl get ingress
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
NAME            CLASS    HOSTS                        ADDRESS          PORTS   AGE
nginx-ingress   <none>   carlos.student.lasalle.com   192.168.99.100   80      35m
```
Si ahora accedieramos directamente a carlos.student.lasalle.com nos fallaría porque intentaria buscarlo en internet, por eso lo qe vamos a hacer es asociar ese host a la misma ip 192.168.99.100:
  
  ```
sudo nano /etc/hosts            
#Prácticas máster:
192.168.99.100 carlos.student.lasalle.com
```
 Comprobamos:
 
 ```
curl http://carlos.student.lasalle.com

<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
![alt text](imagenes/ingress1.png)

* Una vez realizadas las pruebas con el protocolo HTTP, se pide acceder al servicio mediante la utilización del protocolo HTTPS, para ello:

 Crear un certificado mediante la herramienta OpenSSL u otra similar:
 
 ```
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout privatekey.key -out publicert.cert -subj "/CN=carlos.student.lasalle.com/O=carlos.student.lasalle.com" 
Generating a 2048 bit RSA private key
..+++
...................................................................+++
writing new private key to 'privatekey.key'
-----
ls                                  
answer_exercise_1.md	deployment1.yml		ingress1.yml		service1.yml
answer_exercise_2.md	grabacionpantalla.txt	privatekey.key
answer_exercise_3.md	imagenes		publicert.cert

```
 

• Crear un secret que contenga el certificado

```
apiVersion: v1
kind: Secret
metadata:
  name: tls-secret
data:
  tls.crt: |
    MIIDEDCCAfgCCQDc3W8Um4pPPzANBgkqhkiG9w0BAQsFADBKMSMwIQYDVQQDDBpj
    YXJsb3Muc3R1ZGVudC5sYXNhbGxlLmNvbTEjMCEGA1UECgwaY2FybG9zLnN0dWRl
    bnQubGFzYWxsZS5jb20wHhcNMjAxMTIyMTkxOTM3WhcNMjExMTIyMTkxOTM3WjBK
    MSMwIQYDVQQDDBpjYXJsb3Muc3R1ZGVudC5sYXNhbGxlLmNvbTEjMCEGA1UECgwa
    Y2FybG9zLnN0dWRlbnQubGFzYWxsZS5jb20wggEiMA0GCSqGSIb3DQEBAQUAA4IB
    DwAwggEKAoIBAQDZHM7QOCAxpQhPAjp4EubOZmTQ1+BYTMmVTVJl6QwSaCHjmdaV
    vSubic7/EDLRo4sY5iOCMAcvr+fFc2EpYvfJCXw2km52brAJPBG6gUW87LesBKW+
    3y9KGuY98qynB7fijLhN7b1O6eFH7TszzArPerOiBkMYMYlfyu+/+wbqYH0fnDLh
    YjTV8huioFc1NbZe9Ps6w8Goz/nPPHpzAH6e345J4rjjtWXH4fx62KGaGesOHK0U
    NWSRW9PPyNHhFDaY6xobrutS2Eouc5NidI0kPAIIoAugO5XtQagD72N+59IktxD/
    QJ+WlQ58yHV3o4qE16YTcKzA5bRdhcKIOcytAgMBAAEwDQYJKoZIhvcNAQELBQAD
    ggEBAD6nOAPEyWBB//EZKTPXua6O/bJn2TGTVtYoGJMQKFv6VpoVFRNOxOuMipUA
    TeCdt63E7DEQcnDKuPwlLaG1UoiZL0ZOiLKp/6tgaS7dj2D88EPXrQ+lFUaGiy1o
    FCXgRpRMHfp3BrsdEbhkDvcHvFO6FPUX66tR5oggLyoiqiyzuAId4GS2GJYEB/HU
    NSYHUfdP5SuDW8kMhN/TCz7OzOO16fgQL33tTO7WDytEfqj4CEuxhsH3RsPsb7lm
    9tBa0Z2bH5NESw1OzoQBVsG05CLrF/dXqthtfD6MJKufVgQL7Fvy9tcPWck48i3K
    crOzIGzG8uGagr9AwPxbOsloVTk=
  tls.key: |
    MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQDZHM7QOCAxpQhP
    Ajp4EubOZmTQ1+BYTMmVTVJl6QwSaCHjmdaVvSubic7/EDLRo4sY5iOCMAcvr+fF
    c2EpYvfJCXw2km52brAJPBG6gUW87LesBKW+3y9KGuY98qynB7fijLhN7b1O6eFH
    7TszzArPerOiBkMYMYlfyu+/+wbqYH0fnDLhYjTV8huioFc1NbZe9Ps6w8Goz/nP
    PHpzAH6e345J4rjjtWXH4fx62KGaGesOHK0UNWSRW9PPyNHhFDaY6xobrutS2Eou
    c5NidI0kPAIIoAugO5XtQagD72N+59IktxD/QJ+WlQ58yHV3o4qE16YTcKzA5bRd
    hcKIOcytAgMBAAECggEARxfb0OrP//GeauC77J2j3mpBEPHfkIp0Z+NN2/kX7t8S
    WckCLz4mjWPu2C6htWvsHFcfwX2gnMjpCArkAf1aTgZBGk2Qxk8wbZWx6HR0NYz+
    B+HttDa48XcldZuFVZx9Zj3WDA6RMsn67uaLwEZMM+xl7eRHDCEMbsWxBBhbB1u+
    pgTYKEpknjanXCzxfDxv3b1V/IGVwN1PBZSPGNCVUaLLfsk4QZJ8NyLAsJvlf5C/
    XuS55QxZwzzrZvY7jD6bnnrJQ79R9c5J/xGTRUSVTLEdspr595hXdPxzFNcGz4l/
    rewYLyr2B+SqFFDVwO4BgrXFQ5J1mhXGIZcDcDGFAQKBgQDyzpz88e8oGz6PQZKI
    mRthbIrRmOIdS3/FuI9enM+fUKlWgdTJnrB/AM+2NB++NwWn0ZphFxsazK7foNmN
    qWU5BFPU8IQJ47wxyAgrOFPihLrpnJzE6rmKT3NFTuX6hmm1S4r7jC7ISexGrPMC
    dE/iV27TtW8I32w8vRAL7p8c8QKBgQDk6Mor5rwLn+Stgipq0fO6UaSSNJFO07Wm
    hsSHkIdDAcSvuOtyaNJZIpqy1t4IzabD+vHxyHHEW3ndkSntlQoVVJL6jqZZ24Ti
    LX+omIu2TGaVidPi/4lnsZwXZRYPIyXMZ6grdKePySrtl35LPnNcA1T4z5NOIJxw
    6AVsdS9bfQKBgQDUGs1k4WuL25Dgz+VT0nvqtUjnI+iSsrthW4JSoiV1BJ+I5E2f
    +zIvH+RTRGFeZu5X1XBI1EhnNtqkm/ywFYRJNJCO+bQ+d2SVECqipsgQnuYw4RDJ
    rEp8mj8WEoAwQ27gYqd8as8LRBhU/Qwj3rtR/BLieiIO3C3POVFlOwRf4QKBgQCk
    QO4hBlXUuYzre8VYFQMgpMjEKHhD6eIkdfu7ktecGz65IKXB2LlnQE9lGI0GIBsT
    8ZN+fOg1j7YoaKt/UTyFBcTZiVaQj9YQPbmmjp9h1fyePOIPtnEwON/aYayse/n5
    bD/dvr0ljaICDnyPHWbFlZZ5WulGXFwlIDgUFRpAOQKBgHmgGx68XxYiq2C+eofE
    cBxDkQTPawvUj4a2xysKds9WjcNj9kqtRWLDTwWxoIB+5snFY8042sZtG6vhB3sf
    OMs5p1zprYdcvX6gXjkVgkfhznoIv6qgJ/j7zOF0BbJ7U0MuC1k8WQb/EzmFgB7u
    vaz4tbINlEStt1BMQtAntTTw
type: kubernetes.io/tls
```

```
kubectl apply -f secret.yml
secret/tls-secret created

kubectl get secret tls-secret                                                                                                                                           master 
NAME         TYPE                DATA   AGE
tls-secret   kubernetes.io/tls   2      14m
```

Redesplegamos el servicio con el nombre del secret para nuestro host:

```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: nginx-ingress
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /$1
spec:
  tls:
    - hosts:
        - carlos.student.lasalle.com
      secretName: tls-secret
  rules:
    - host: carlos.student.lasalle.com
      http:
        paths:
          - path: /
            pathType: Prefix
            backend:
              service:
                name: nginx-service
                port:
                  number: 80
```

```
kubectl apply -f ingress1.yml
ingress.networking.k8s.io/nginx-ingress configured
```


Comprobamos que corre ahora tambien en el 443:

```
kubectl get ingress
Warning: extensions/v1beta1 Ingress is deprecated in v1.14+, unavailable in v1.22+; use networking.k8s.io/v1 Ingress
NAME            CLASS    HOSTS                        ADDRESS          PORTS     AGE
nginx-ingress   <none>   carlos.student.lasalle.com   192.168.99.100   80, 443   70m
```

y acabamos de validar:

No sé porqué aún activando correctamente tls y corriendo en el 443, a la hora de mostrar el certificado me lo esta cogiendo otro nombre:

![alt text](imagenes/certificado.png)

