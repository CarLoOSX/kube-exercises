> Crear un objeto de tipo deployment con las especificaciones del ejercicio 1.

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          resources:
            limits:
              cpu: "100m"
              memory: "256Mi"
            requests:
              cpu: "100m"
              memory: "256Mi"
```


* Despliega una nueva versión de tu nuevo servicio mediante la técnica “recreate”

```
kubectl apply -f deployment4.yml                                                                                                                                            master 
deployment.apps/nginx-deployment created

kubectl get pods                                                                                                                                                            master 
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-f6fbbb77c-7w5kz   1/1     Running   0          27s
nginx-deployment-f6fbbb77c-l84r8   1/1     Running   0          27s
nginx-deployment-f6fbbb77c-qwqz9   1/1     Running   0          27s

kubectl delete pods --all                                                                                                                                                   master 
pod "nginx-deployment-f6fbbb77c-7w5kz" deleted
pod "nginx-deployment-f6fbbb77c-l84r8" deleted
pod "nginx-deployment-f6fbbb77c-qwqz9" deleted

kubectl get pods                                                                                                                                                            master 
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-f6fbbb77c-2bdms   1/1     Running   0          19s
nginx-deployment-f6fbbb77c-d4g8d   1/1     Running   0          19s
nginx-deployment-f6fbbb77c-m8mg2   1/1     Running   0          19s

```

Al parecer al no pasarle flags de recreate me ha hecho lo siguiente ya que el valor por defecto es "RollingUpdate"

```
Estrategia    
   .spec.strategy especifica la estrategia usada para remplazar los Pods viejos con los nuevos. .spec.strategy.type puede tener el valor "Recreate" o "RollingUpdate". "RollingUpdate" el valor predeterminado.
```

Nosotros debemos aplicar el siguiente:

```
Despliegue mediante recreación

Todos los Pods actuales se eliminan antes de que los nuevos se creen cuando .spec.strategy.type==Recreate.
```
Así que nuestro fichero yml queda así:

```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          resources:
            limits:
              cpu: "100m"
              memory: "256Mi"
            requests:
              cpu: "100m"
              memory: "256Mi"
```
Añadiendo :

```
  strategy:
    type: Recreate
```

Volvemos a probar pero antes eliminamos el deployment mediante kubectl:

```
kubectl delete deployment --all                                                                                                                                             master 
deployment.apps "nginx-deployment" deleted

```
Eso ya borra rs y pods, desplegamos de nuevo:

```
kubectl apply -f deployment4.yml                                                                                                                                            master 
deployment.apps/nginx-deployment created
kubectl get pods                                                                                                                                                            master 
NAME                               READY   STATUS    RESTARTS   AGE
nginx-deployment-f6fbbb77c-ffcxb   1/1     Running   0          14s
nginx-deployment-f6fbbb77c-jqp4c   1/1     Running   0          14s
nginx-deployment-f6fbbb77c-ss6zf   1/1     Running   0          14s
```

Comprobamos:

```
kubectl describe deployment                                                                                                                                                 master 
Name:               nginx-deployment
Namespace:          default
CreationTimestamp:  Mon, 16 Nov 2020 23:09:58 +0100
Labels:             app=nginx-server
Annotations:        deployment.kubernetes.io/revision: 1
Selector:           app=nginx-server
Replicas:           3 desired | 3 updated | 3 total | 3 available | 0 unavailable
StrategyType:       Recreate
MinReadySeconds:    0
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       <none>
    Host Port:  <none>
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-f6fbbb77c (3/3 replicas created)
Events:
  Type    Reason             Age   From                   Message
  ----    ------             ----  ----                   -------
  Normal  ScalingReplicaSet  2m3s  deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 3
```

* Despliega una nueva versión haciendo “rollout deployment”

Lo primero de todo sería quitar la strategia de recreate ya que queremos el valor por defecto de "RollingUpdate", eliminamos el deployment actual y volvemos a desplegar.

Ahora bien, supongamos que hemos actualizado nuestra plantilla del pod, como en mi caso he subido a 5 replicas, con esto hariamos que se actualizase y veremos que tenemos un deployment nuevo, vamos a comprobarlo:

```
kubectl get deployments                                                                                                                                                     master 
NAME               READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment   3/3     3            3           15s

kubectl apply -f deployment4.yml                                                                                                                                            master 
deployment.apps/nginx-deployment configured

kubectl rollout status deployment nginx-deployment                                                                                                                          master 
Waiting for deployment "nginx-deployment" rollout to finish: 3 of 5 updated replicas are available...
Waiting for deployment "nginx-deployment" rollout to finish: 4 of 5 updated replicas are available...
deployment "nginx-deployment" successfully rolled out                                                                                

kubectl get pods -o wide                                                                                                                                                    master 
NAME                               READY   STATUS    RESTARTS   AGE   IP           NODE       NOMINATED NODE   READINESS GATES
nginx-deployment-f6fbbb77c-5tkjp   1/1     Running   0          93s   172.17.0.7   minikube   <none>           <none>
nginx-deployment-f6fbbb77c-7gcwt   1/1     Running   0          69s   172.17.0.9   minikube   <none>           <none>
nginx-deployment-f6fbbb77c-bmdw4   1/1     Running   0          69s   172.17.0.8   minikube   <none>           <none>
nginx-deployment-f6fbbb77c-c7dwm   1/1     Running   0          93s   172.17.0.5   minikube   <none>           <none>
nginx-deployment-f6fbbb77c-djtxw   1/1     Running   0          93s   172.17.0.6   minikube   <none>           <none>
```

como se puede ver he subido sin problemas a 5 replicas:
```
kubectl get rs                                                                                                                                                         1   master 
NAME                         DESIRED   CURRENT   READY   AGE
nginx-deployment-f6fbbb77c   5         5         5       12m
```
Aún así al hacer "describe" me seguia diciendo revision 1 así que he cambiado la imagen directamente por comando con "--record=true" ya que parece ser que es así como funciona:

 kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:1.91 --record=true     
 
 y ahora sí al hacer describe ya tengo revision 2:
 
 ```
kubectl describe deployment                                                                                                                                                 master 
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Tue, 17 Nov 2020 00:09:11 +0100
Labels:                 app=nginx-server
Annotations:            deployment.kubernetes.io/revision: 2
                        kubernetes.io/change-cause: kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:1.91 --record=true
Selector:               app=nginx-server
Replicas:               5 desired | 3 updated | 7 total | 4 available | 3 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.91
    Port:       <none>
    Host Port:  <none>
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    ReplicaSetUpdated
OldReplicaSets:  nginx-deployment-f6fbbb77c (4/4 replicas created)
NewReplicaSet:   nginx-deployment-675b8d89b7 (3/3 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  8m51s  deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 3
  Normal  ScalingReplicaSet  8m6s   deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 5
  Normal  ScalingReplicaSet  3m32s  deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 2
  Normal  ScalingReplicaSet  3m32s  deployment-controller  Scaled down replica set nginx-deployment-f6fbbb77c to 4
  Normal  ScalingReplicaSet  3m32s  deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 3
```

* Realiza un rollback a la versión generada previamente


Primero de todo vemos el historial de versiones:

```
kubectl rollout history deployment.v1.apps/nginx-deployment                                                                                                                 master 
deployment.apps/nginx-deployment 
REVISION  CHANGE-CAUSE
1         kubectl apply --filename=deployment4.yml --record=true
2         kubectl set image deployment.v1.apps/nginx-deployment nginx=nginx:1.91 --record=true
```

Al ya tener un rev 2, puedo volver a la revision 1 de la siguiente manera:

```
kubectl rollout undo deployment.v1.apps/nginx-deployment --to-revision=1                                                                                                    master 
deployment.apps/nginx-deployment rolled back

kubectl describe deployment                                                                                                                                                 master 
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Tue, 17 Nov 2020 00:09:11 +0100
Labels:                 app=nginx-server
Annotations:            deployment.kubernetes.io/revision: 3
                        kubernetes.io/change-cause: kubectl apply --filename=deployment4.yml --record=true
Selector:               app=nginx-server
Replicas:               5 desired | 5 updated | 5 total | 4 available | 1 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       <none>
    Host Port:  <none>
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    ReplicaSetUpdated
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-f6fbbb77c (5/5 replicas created)
Events:
  Type    Reason             Age               From                   Message
  ----    ------             ----              ----                   -------
  Normal  ScalingReplicaSet  11m               deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 3
  Normal  ScalingReplicaSet  6m20s             deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 2
  Normal  ScalingReplicaSet  6m20s             deployment-controller  Scaled down replica set nginx-deployment-f6fbbb77c to 4
  Normal  ScalingReplicaSet  6m20s             deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 3
  Normal  ScalingReplicaSet  6s (x2 over 10m)  deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 5
  Normal  ScalingReplicaSet  6s                deployment-controller  Scaled down replica set nginx-deployment-675b8d89b7 to 0
```
Como se puede ver me ha creado el revision 3 como consecuencia de volver al revision 1 he visto que hay una réplica que sigue en pending así que vamos a redesplegar


```
kubectl apply -f deployment4.yml                                                                                                                                            master 
deployment.apps/nginx-deployment configured

kubectl rollout status deployment nginx-deployment                                                                                                                          master 
deployment "nginx-deployment" successfully rolled out

kubectl describe deployment                                                                                                                                                 master 
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Tue, 17 Nov 2020 00:09:11 +0100
Labels:                 app=nginx-server
Annotations:            deployment.kubernetes.io/revision: 3
                        kubernetes.io/change-cause: kubectl apply --filename=deployment4.yml --record=true
Selector:               app=nginx-server
Replicas:               5 desired | 5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       <none>
    Host Port:  <none>
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-f6fbbb77c (5/5 replicas created)
Events:
  Type    Reason             Age                 From                   Message
  ----    ------             ----                ----                   -------
  Normal  ScalingReplicaSet  13m                 deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 3
  Normal  ScalingReplicaSet  8m10s               deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 2
  Normal  ScalingReplicaSet  8m10s               deployment-controller  Scaled down replica set nginx-deployment-f6fbbb77c to 4
  Normal  ScalingReplicaSet  8m10s               deployment-controller  Scaled up replica set nginx-deployment-675b8d89b7 to 3
  Normal  ScalingReplicaSet  116s (x2 over 12m)  deployment-controller  Scaled up replica set nginx-deployment-f6fbbb77c to 5
  Normal  ScalingReplicaSet  116s                deployment-controller  Scaled down replica set nginx-deployment-675b8d89b7 to 0
```

Ya están de nuevo las 5 réplicas funcionando así que sin problemas, como nota también podría haber actualizado el número de replicas por comando con el --record, o directamente ya sabiendo correctamente como funciona podría haber puesto el ejercicio en limpio pero así me parece más auténtico, eso sí, el número de replicas ya lo he dejado en 5 porque no he vuelto a cambiarlo a 3.
