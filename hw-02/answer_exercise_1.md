>1.Crea un pod de forma declarativa con las siguientes especificaciones:

```
apiVersion: v1
kind: Pod
metadata:
  name: ex1pod
  labels:
    app: nginx-server
spec:
  containers:
    - name: nginx
      image: nginx:1.19.4
      ports:
        - containerPort: 80
      resources:
        limits:
          cpu: "100m"
          memory: "256Mi"
        requests:
          cpu: "100m"
          memory: "256Mi"
```
> Realiza un despliegue en Kubernetes, y responde las siguientes preguntas:

* ¿Cómo puedo obtener las últimas 10 líneas de la salida estándar (logs generados por la aplicación)?
 ```
kubectl logs ex1pod --tail=10                                                                                                                                                   master 
/docker-entrypoint.sh: /docker-entrypoint.d/ is not empty, will attempt to perform configuration
/docker-entrypoint.sh: Looking for shell scripts in /docker-entrypoint.d/
/docker-entrypoint.sh: Launching /docker-entrypoint.d/10-listen-on-ipv6-by-default.sh
10-listen-on-ipv6-by-default.sh: Getting the checksum of /etc/nginx/conf.d/default.conf
10-listen-on-ipv6-by-default.sh: Enabled listen on IPv6 in /etc/nginx/conf.d/default.conf
/docker-entrypoint.sh: Launching /docker-entrypoint.d/20-envsubst-on-templates.sh
/docker-entrypoint.sh: Configuration complete; ready for start up
```


* ¿Cómo podría obtener la IP interna del pod? Aporta capturas para indicar el proceso que seguirías.

```
kubectl describe pods ex1pod                                                                                                                                                    master 
Name:         ex1pod
Namespace:    default
Priority:     0
Node:         minikube/192.168.99.100
Start Time:   Mon, 16 Nov 2020 14:42:03 +0100
Labels:       app=nginx-server
Annotations:  <none>
Status:       Running
IP:           172.17.0.5
IPs:
  IP:  172.17.0.5
```

* ¿Qué comando utilizarías para entrar dentro del pod?
```
kubectl exec -it ex1pod bash                                                                                                                                               1   master 
kubectl exec [POD] [COMMAND] is DEPRECATED and will be removed in a future version. Use kubectl exec [POD] -- [COMMAND] instead.
root@ex1pod:/# 
```

 Comando deprecado pero todavía vale.


* Necesitas visualizar el contenido que expone NGINX, ¿qué acciones
debes llevar a cabo?

Haciendo un describe veo que el puerto interno es el 80 pero no tengo ningun puerto asociado a este:

```
Containers:
  nginx:
    Container ID:   docker://9994c383f5daa653f93bed6543e6797f69de857a10df374198590e7ae9bc59d6
    Image:          nginx:1.19.4
    Image ID:       docker-pullable://nginx@sha256:aeade65e99e5d5e7ce162833636f692354c227ff438556e5f3ed0335b7cc2f1b
    Port:           80/TCP
    Host Port:      0/TCP
    State:          Running
      Started:      Mon, 16 Nov 2020 14:42:21 +0100
```

Aqui lo que haria seria exponer el puerto de forma declarativa en el yml:

primero eliminamos el pod
```
kubectl delete pod ex1pod 
```
despues modificamos el puerto host "hostPort" en el yml
```
apiVersion: v1
kind: Pod
metadata:
  name: ex1pod
  labels:
    app: nginx-server
spec:
  containers:
    - name: nginx
      image: nginx:1.19.4
      ports:
        - containerPort: 80  
          hostPort: 8080  
      resources:
        limits:
          cpu: "100m"
          memory: "256Mi"
        requests:
          cpu: "100m"
          memory: "256Mi"
```
volvemos a arrancar y comprobamos 
```
kubectl describe pods ex1pod                                                                                                                                                    master 
Name:         ex1pod
Namespace:    default
Priority:     0
Node:         minikube/192.168.99.100
Start Time:   Mon, 16 Nov 2020 16:01:45 +0100
Labels:       app=nginx-server
Annotations:  <none>
Status:       Running
IP:           172.17.0.5
IPs:
  IP:  172.17.0.5
Containers:
  nginx:
    Container ID:   docker://d505b57a0a587ee5940b7bd4ee25cd7c447410407b5671d5815bfd7265069665
    Image:          nginx:1.19.4
    Image ID:       docker-pullable://nginx@sha256:aeade65e99e5d5e7ce162833636f692354c227ff438556e5f3ed0335b7cc2f1b
    Port:           80/TCP
    Host Port:      8080/TCP
    State:          Running
```

entramos en minikube y vemos que el NAT se ha realizado corractamente haciendo un curl en localhost con el puerto especificado anteriormente:

```
minikube ssh                                                                                                                                                          14   master 
                         _             _            
            _         _ ( )           ( )           
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ curl localhost:8080
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
$ 
```


* Indica la calidad de servicio (QoS) establecida en el pod que acabas de
crear. ¿Qué lo has mirado?

Entiendo que lo que se quiere ver en este ejercicio es que el pod esta en uno de estos 3 estados como dice en la pagina oficial de la documentacion de k8s:
Teniendo en cuenta los limites que hemos establecido
```
QoS classes

When Kubernetes creates a Pod it assigns one of these QoS classes to the Pod:
Guaranteed
Burstable
BestEffort
```
se podría especificar como uno de estos 3 de la siguiente manera:
```
status:
  qosClass: BestEffort
```
cada uno tiene una finalidad especifica por ejemplo

```
For a Pod to be given a QoS class of BestEffort, the Containers in the Pod must not have any memory or CPU limits or requests.
```

Utilizando el mismo comando de siempre "describe" podemos ver el estado, que es el que aplica por defecto:

```
kubectl describe pod ex1pod | grep QoS                                                                                                                                      master 
QoS Class:       Guaranteed
```
Como era de esperar en nuestro caso el aplicado es Guaranteed ya que cumple lo siguiente:

```
For a Pod to be given a QoS class of Guaranteed:

Every Container, including init containers, in the Pod must have a memory limit and a memory request, and they must be the same.
Every Container, including init containers, in the Pod must have a CPU limit and a CPU request, and they must be the same.
Here is the configuration file for a Pod that has one Container. The Container has a memory limit and a memory request, both equal to 200 MiB. The Container has a CPU limit and a CPU request, both equal to 700 milliCPU:
```