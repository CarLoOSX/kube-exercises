> Diseña una estrategia de despliegue que se base en ”Blue Green”. Podéis
utilizar la imagen del ejercicio 1.
* Existe una aplicación que está desplegada en el clúster (en el ejemplo, 1.0v):

* Antes de ofrecer el servicio a los usuarios, la compañía necesita realizar una serie de validaciones con la versión 2.0. Los usuarios siguen accediendo a la versión 1.0:

* Una vez que el equipo ha validado la aplicación, se realiza un switch del tráfico a la versión 2.0 sin impacto para los usuarios:

Para este ejercicio he usado el deployment del ejercicio anterior y le he llamado green.yml, a diferencia del ejercicio anterior este tiene:

```
version: 1.0
```

```
kubectl describe deployment
Name:                   nginx-deployment
Namespace:              default
CreationTimestamp:      Wed, 18 Nov 2020 22:03:05 +0100
Labels:                 app=nginx-server
Annotations:            deployment.kubernetes.io/revision: 1
Selector:               app=nginx-server
Replicas:               5 desired | 5 updated | 5 total | 5 available | 0 unavailable
StrategyType:           RollingUpdate
MinReadySeconds:        0
RollingUpdateStrategy:  25% max unavailable, 25% max surge
Pod Template:
  Labels:  app=nginx-server
           version=1.0
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       <none>
    Host Port:  <none>
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Conditions:
  Type           Status  Reason
  ----           ------  ------
  Available      True    MinimumReplicasAvailable
  Progressing    True    NewReplicaSetAvailable
OldReplicaSets:  <none>
NewReplicaSet:   nginx-deployment-7fdbb8756b (5/5 replicas created)
Events:
  Type    Reason             Age    From                   Message
  ----    ------             ----   ----                   -------
  Normal  ScalingReplicaSet  3m12s  deployment-controller  Scaled up replica set nginx-deployment-7fdbb8756b to 5
```

En este momento valido desde minikube que no tengo acceso a los pods:

```
minikube ssh
                         _             _            
            _         _ ( )           ( )           
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ curl localhost:80
curl: (7) Failed to connect to localhost port 80: Connection refused
$ 
```

He levantado un servicio que apunta a ese deployment para hacerlo accesible:

```
ubectl apply -f serviceBlue.yml   
service/nginx created
           
kubectl describe svc nginx
Name:                     nginx
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx-server,version=1.0
Type:                     LoadBalancer
IP:                       10.96.0.123
Port:                     http  80/TCP
TargetPort:               80/TCP
NodePort:                 http  31386/TCP
Endpoints:                172.17.0.3:80,172.17.0.4:80,172.17.0.5:80 + 2 more...
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>


```
Despues de ver la versión correcta, comprobamos que puedo acceder desde la ip especificada:

```
minikube service nginx --url
http://192.168.99.100:31386

minikube ssh
                         _             _            
            _         _ ( )           ( )           
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ curl 10.102.54.248
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
$ 
```
![alt text](imagenes/nueva-ip.png)


La versión 1 tiene 100 milicores y la 2 200 milicores.
Comprobamos los de la 1, se pintan 10 pero porque cpu esta repetido 2 veces por cada pod (5):

```
kubectl describe pods |grep cpu
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
```

Ahora vamos a desplegar nuestro deployment verde (2.0) con 200 milicores y comprobamos los pods que estan corriendo:

```
kubectl apply -f green.yml
deployment.apps/nginx-deployment2 created

kubectl describe pods |grep cpu
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     100m
      cpu:        100m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m

```

Como vemos es correcto, ahora vamos a configurar un service para que nuestro qa pueda probar, con un nuevo nombre:
Cambiamos el clusterIp en este momento y le ponemos 10.96.0.123 -> 10.96.0.124 pero dejamos la misma ip porque esperamos que nos de otro puerto

```
apiVersion: v1
kind: Service
metadata:
  name: nginx2
  namespace: default
spec:
  ports:
    - name: http
      port: 80
  selector:
    app: nginx-server
    version: "2.0"
  clusterIP: 10.96.0.124
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
      - ip: 192.168.99.100
```

Comprobamos que se cree correctamente y obtenemos nuevo puerto para el QA

```
minikube service nginx2 --url
http://192.168.99.100:32162
```
![alt text](imagenes/servicio2Blue.png)


Ahora llega el paso en el que vamos a apuntar nuestro sercicio blue al nuevo deployment...
¿ Cómo ?
Una manera sería cambiar la versión de la app de nustro servicioBlue.yml, nuestro primer servicio:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
spec:
  ports:
    - name: http
      port: 80
  selector:
    app: nginx-server
    version: "2.0"
  clusterIP: 10.96.0.123
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
      - ip: 192.168.99.100
```
Comprobamos:

````
kubectl apply -f serviceBlue.yml
service/nginx configured

 kubectl describe svc nginx
Name:                     nginx
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx-server,version=2.0
Type:                     LoadBalancer
IP:                       10.96.0.123
Port:                     http  80/TCP
TargetPort:               80/TCP
NodePort:                 http  30694/TCP
Endpoints:                172.17.0.10:80,172.17.0.11:80
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
````

Aquí ya vemos que estamos apuntando a la versión 2 por tanto vamos a quitar el deployment de la versión 1 y el servicio de la versión 2 que hemos hecho para nuestro QA:

````
kubectl get deployment                
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment    5/5     5            5           19m
nginx-deployment2   5/5     5            5           13m

kubectl delete deployment nginx-deployment
deployment.apps "nginx-deployment" deleted

kubectl get deployment 
NAME                READY   UP-TO-DATE   AVAILABLE   AGE
nginx-deployment2   5/5     5            5           14m

kubectl describe pods |grep cpu
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m
      cpu:     200m
      cpu:        200m

kubectl get svc
NAME         TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP      10.96.0.1     <none>        443/TCP        2d
nginx        LoadBalancer   10.96.0.123   <pending>     80:30694/TCP   21m
nginx2       LoadBalancer   10.96.0.124   <pending>     80:32162/TCP   11m

kubectl delete svc nginx2
service "nginx2" deleted

kubectl get svc  
NAME         TYPE           CLUSTER-IP    EXTERNAL-IP   PORT(S)        AGE
kubernetes   ClusterIP      10.96.0.1     <none>        443/TCP        2d
nginx        LoadBalancer   10.96.0.123   <pending>     80:30694/TCP   21m

````

Ahora que ya hemos borrado tanto el servicioGreen(nginx2) como el deploymentBlue(nginx-deployment) comprobamos :

![alt text](imagenes/final.png)

Como se puede ver ya no se puede acceder desde el nuevo puerto, el de nuestro QA, porque hemos eliminado el servicio y ya no existen los pods con 100 milicores ni existen deployments.

Esto sería todo, no sé si es la mejor manera pero es la que se ma ha ocurrido.


