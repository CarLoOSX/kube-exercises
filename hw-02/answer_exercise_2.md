> Crear un objeto de tipo replicaSet a partir del objeto anterior con las siguientes especificaciones:

* Debe tener 3 replicas

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: ex2pod
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          ports:
            - containerPort: 80
              hostPort: 8080
          resources:
            limits:
              cpu: "100m"
              memory: "256Mi"
            requests:
              cpu: "100m"
              memory: "256Mi"
```
Comprobamos con el comando describe como en el ejercicio anterior que es el que mas información otorga:
```
kubectl describe rs ex2pod                                                                                                                                                  master 
Name:         ex2pod
Namespace:    default
Selector:     app=nginx-server
Labels:       app=nginx-server
Annotations:  <none>
Replicas:     3 current / 3 desired
Pods Status:  1 Running / 2 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       80/TCP
    Host Port:  8080/TCP
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age   From                   Message
  ----    ------            ----  ----                   -------
  Normal  SuccessfulCreate  88s   replicaset-controller  Created pod: ex2pod-9vw5f
  Normal  SuccessfulCreate  88s   replicaset-controller  Created pod: ex2pod-rhkmw
  Normal  SuccessfulCreate  88s   replicaset-controller  Created pod: ex2pod-5pxsz
```

* ¿Cúal sería el comando que utilizarías para escalar el número de replicas a
10?

```
kubectl scale --replicas=10 rs/ex2pod                                                                                                                                        master 
replicaset.apps/ex2pod scaled
```
Comprobamos de nuevo:

```
kubectl describe rs ex2pod                                                                                                                                                  master 
Name:         ex2pod
Namespace:    default
Selector:     app=nginx-server
Labels:       app=nginx-server
Annotations:  <none>
Replicas:     10 current / 10 desired
Pods Status:  1 Running / 9 Waiting / 0 Succeeded / 0 Failed
Pod Template:
  Labels:  app=nginx-server
  Containers:
   nginx:
    Image:      nginx:1.19.4
    Port:       80/TCP
    Host Port:  8080/TCP
    Limits:
      cpu:     100m
      memory:  256Mi
    Requests:
      cpu:        100m
      memory:     256Mi
    Environment:  <none>
    Mounts:       <none>
  Volumes:        <none>
Events:
  Type    Reason            Age              From                   Message
  ----    ------            ----             ----                   -------
  Normal  SuccessfulCreate  4m9s             replicaset-controller  Created pod: ex2pod-9vw5f
  Normal  SuccessfulCreate  4m9s             replicaset-controller  Created pod: ex2pod-rhkmw
  Normal  SuccessfulCreate  4m9s             replicaset-controller  Created pod: ex2pod-5pxsz
  Normal  SuccessfulDelete  49s              replicaset-controller  Deleted pod: ex2pod-rhkmw
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-pnp5n
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-57scp
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-6ljq2
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-2ldlx
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-gzx4p
  Normal  SuccessfulCreate  5s               replicaset-controller  Created pod: ex2pod-9xrgz
  Normal  SuccessfulCreate  5s (x2 over 5s)  replicaset-controller  (combined from similar events): Created pod: ex2pod-p7bm5
```

* Si necesito tener una replica en cada uno de los nodos de Kubernetes, ¿qué objeto se adaptaría mejor? (No es necesario adjuntar el objeto)

Según la documentación oficial de k8s utilizaría DaemonSet:

```
DaemonSet
Un DaemonSet garantiza que todos (o algunos) de los nodos ejecuten una copia de un Pod. Conforme se añade más nodos al clúster, nuevos Pods son añadidos a los mismos. Conforme se elimina nodos del clúster, dichos Pods se destruyen. Al eliminar un DaemonSet se limpian todos los Pods que han sido creados.
```

Ahora mismo solo disponemos de un nodo:
Node:         minikube/192.168.99.100

```
kubectl get nodes                                                                                                                                                           master 
   NAME       STATUS   ROLES    AGE     VERSION
   minikube   Ready    master   5d20h   v1.19.2
```