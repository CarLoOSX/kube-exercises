> Crea un objeto de tipo service para exponer la aplicación del ejercicio
anterior de las siguientes formas:

* Exponiendo el servicio hacia el exterior (crea service1.yaml)

hay diferentes tipos de servicio:

NodePort:
ClusterIp:
LoadBalancer:

Para este punto utilizaremos nodePort:

```
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
spec:
  type: NodePort
  ports:
    - name: http
      port: 8080
      targetPort: http
  selector:
    app: nginx-server
```

para empezar lo q he tenido que hacer es recrear de nuevo el replicaset ya que como estaba exponiendo el hostPoty los pods se quedaban en estado "pending" y solo estaba en "running" uno de ellos que es el que que ocupaba el puerto hostport.

```
apiVersion: apps/v1
kind: ReplicaSet
metadata:
  name: ex2pod
  labels:
    app: nginx-server
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx-server
  template:
    metadata:
      labels:
        app: nginx-server
    spec:
      containers:
        - name: nginx
          image: nginx:1.19.4
          ports:
            - containerPort: 80
          resources:
            limits:
              cpu: "100m"
              memory: "256Mi"
            requests:
              cpu: "100m"
              memory: "256Mi"
```
para ver esto que he explicado, lo he hecho mediante el describe del servicio donde he visto que no habia ningun endpoint

```
kubectl describe svc nginx                                                                                                                                                  master 
Name:                     nginx
Namespace:                default
Labels:                   <none>
Annotations:              <none>
Selector:                 app=nginx-server
Type:                     NodePort
IP:                       10.107.197.242
Port:                     http  8080/TCP
TargetPort:               http/TCP
NodePort:                 http  32227/TCP
Endpoints:                <none>
Session Affinity:         None
External Traffic Policy:  Cluster
Events:                   <none>
```

una vez recreado he vuelto a pobar redesplegando de nuevo el servicio:

```
y sigo viendo lo mismo ... así que el fallo no va por ahí
```

volvemos a comprobar RS:

```kubectl describe rs ex2pod                                                                                                                                                  master 
   Name:         ex2pod
   Namespace:    default
   Selector:     app=nginx-server
   Labels:       app=nginx-server
   Annotations:  <none>
   Replicas:     3 current / 3 desired
   Pods Status:  3 Running / 0 Waiting / 0 Succeeded / 0 Failed
   Pod Template:
     Labels:  app=nginx-server
     Containers:
      nginx:
       Image:      nginx:1.19.4
       Port:       80/TCP
       Host Port:  0/TCP
       Limits:
         cpu:     100m
         memory:  256Mi
       Requests:
         cpu:        100m
         memory:     256Mi
       Environment:  <none>
       Mounts:       <none>
     Volumes:        <none>
   Events:
     Type    Reason            Age    From                   Message
     ----    ------            ----   ----                   -------
     Normal  SuccessfulCreate  5m50s  replicaset-controller  Created pod: ex2pod-vtvnx
     Normal  SuccessfulCreate  5m50s  replicaset-controller  Created pod: ex2pod-tmdwv
     Normal  SuccessfulCreate  5m50s  replicaset-controller  Created pod: ex2pod-9fz99
```

Por lo visto el problema estaba en el replica set por una propiedad llamada "targetPort: http" al volver a probar vemos que ahora sí:

```
kubectl describe svc nginx                                                                                                                                                  master 
   Name:                     nginx
   Namespace:                default
   Labels:                   <none>
   Annotations:              <none>
   Selector:                 app=nginx-server
   Type:                     NodePort
   IP:                       10.96.97.83
   Port:                     http  8080/TCP
   TargetPort:               8080/TCP
   NodePort:                 http  30701/TCP
   Endpoints:                172.17.0.5:8080,172.17.0.6:8080,172.17.0.7:8080
   Session Affinity:         None
   External Traffic Policy:  Cluster
   Events:                   <none>
```
 y comprobamos de nuevo:
 
 ```minikube ssh                                                                                                                                                                master 
                             _             _            
                _         _ ( )           ( )           
      ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
    /' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
    | ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
    (_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)
    
    $ curl localhost:8080
    curl: (7) Failed to connect to localhost port 8080: Connection refused
    $ 
```        
Como vemos no hay acceso así que volvemos a investigar y ahora parece ser un problema del puerto 8080 cambiandolo al 80 ya funciona

```
minikube service nginx --url                                                                                                                                                master 
http://192.168.99.100:31429
 carloosx  …  PHD  Ejercicios  kube-exercises  2  minikube service nginx --url | curl                                                                                                                                         master 
curl: try 'curl --help' or 'curl --manual' for more information
 carloosx  …  PHD  Ejercicios  kube-exercises  2  curl http://192.168.99.100:31429                                                                                                                             SIGPIPE  2   master 
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

* De forma interna, sin acceso desde el exterior (crea service2.yaml)

lo mismo que el anterior pero esta vez con ClusterIP

```
 minikube service nginx --url                                                                                                                                                master 
* service default/nginx has no node port
 carloosx  …  PHD  Ejercicios  kube-exercises  2  minikube ssh                                                                                                                                                                master 
                         _             _            
            _         _ ( )           ( )           
  ___ ___  (_)  ___  (_)| |/')  _   _ | |_      __  
/' _ ` _ `\| |/' _ `\| || , <  ( ) ( )| '_`\  /'__`\
| ( ) ( ) || || ( ) || || |\`\ | (_) || |_) )(  ___/
(_) (_) (_)(_)(_) (_)(_)(_) (_)`\___/'(_,__/'`\____)

$ curl localhost
curl: (7) Failed to connect to localhost port 80: Connection refused
$ 

```
* Abriendo un puerto especifico de la VM (crea service3.yaml)

volvemos a modificar el service


```
apiVersion: v1
kind: Service
metadata:
  name: nginx
  namespace: default
spec:
  ports:
    - name: http
      port: 80
  selector:
    app: nginx-server
  clusterIP: 10.96.0.123
  type: LoadBalancer
status:
  loadBalancer:
    ingress:
      - ip: 192.168.99.102
```

Asignamos una ip válida ( obtenida previamente con el serivio anterior )

como vemos hemos hecho algunas pruebas hasta que hemos dado con el rango correcto de IP del Cluster:

```
kubectl apply -f service3.yml                                                                                                                                          1   master 
The Service "nginx" is invalid: spec.clusterIP: Invalid value: "10.0.171.239": provided IP is not in the valid range. The range of valid IPs is 10.96.0.0/12
 carloosx  …  PHD  Ejercicios  kube-exercises  2  kubectl apply -f service3.yml                                                                                                                                          1   master 
service/nginx created
```

volvemos a pedir la url del servicio

```
minikube service nginx --url                                                                                                                                                master 
http://192.168.99.100:30854
```
y validamos en el navegador
![alt text](imagenes/ip-servicio.png)


ALTERNATIVA

Paramos service2, volvemos a desplegar service1 y configuramos VirtualBox ya que no necesito crear un service3.yml

comprobamos el puerto:

```minikube service nginx --url                                                                                                                                                master 
   http://192.168.99.100:32711
```
y hacemos nat en la máquina virtual

![alt text](imagenes/servicio-expuesto.png)


